﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Reflection;

namespace Frank.Database.Connection
{
    /// <summary>
    /// Class for database query conditions (e.g. in where clauses)
    /// </summary>
    public class Condition : IConditionItem
    {
        /// <summary>
        /// Build a new object with the given condition item
        /// </summary>
        /// <param name="item">condition item</param>
        public Condition(ConditionItem item) :
            this(Conjunction.Or, item)
        {
        }

        /// <summary>
        /// Build a new object with the given items, all conjuncted with the specified conjunction
        /// </summary>
        /// <param name="conjunction">The conjunction to use</param>
        /// <param name="items">The condition items.</param>
        public Condition(Conjunction conjunction, params IConditionItem[] items)
        {
            Conjunction = conjunction;
            ConditionItems = items;
        }

        /// <summary>
        /// Gets the conjunction
        /// </summary>
        public Conjunction Conjunction
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of condition items
        /// </summary>
        public IList<IConditionItem> ConditionItems
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the conjunction as string
        /// </summary>
        /// <returns></returns>
        public string GetConjunctionString()
        {
            MemberInfo[] memberInfo = Conjunction.GetType().GetMember(Conjunction.ToString());
            object[] attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            return ((DescriptionAttribute)attributes[0]).Description;
        }

        /// <summary>
        /// Gets the parameters for the sql query
        /// </summary>
        public IList<SqlParameter> Parameters
        {
            get
            {
                return ExtractParameters(ConditionItems);
            }
        }

        private IList<SqlParameter> ExtractParameters(IEnumerable<IConditionItem> list)
        {
            List<SqlParameter> result = new List<SqlParameter>();
            foreach (IConditionItem item in list) {
                if (item is Condition) {
                    result.AddRange(ExtractParameters((item as Condition).ConditionItems));
                } else if (item is ConditionItem) {
                    ConditionItem conditionItem = item as ConditionItem;
                    result.Add(new SqlParameter(conditionItem.Parameter, conditionItem.Value));
                }
            }
            return result;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return String.Join(String.Format(" {0} ", GetConjunctionString()), ConditionItems);
        }
    }
}
