﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.ComponentModel;

namespace Frank.Database.Connection
{
    /// <summary>
    /// Enumeration for compare tests in database queries (e.g. in where clauses)
    /// </summary>
    public enum Comparison
    {
        /// <summary>
        /// Lower than
        /// </summary>
        [Description("<")]
        Lesser,

        /// <summary>
        /// Lower or equal than
        /// </summary>
        [Description("<=")]
        LesserOrEqual,

        /// <summary>
        /// Equal
        /// </summary>
        [Description("=")]
        Equal,

        /// <summary>
        /// Not equal
        /// </summary>
        [Description("!=")]
        NotEqual,

        /// <summary>
        /// Greater or equal than
        /// </summary>
        [Description(">=")]
        GreaterOrEqual,

        /// <summary>
        /// Greater than
        /// </summary>
        [Description(">")]
        Greater
    }
}
