﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.ComponentModel;

namespace Frank.Database.Connection
{
    /// <summary>
    /// Enumeration for the conjunction types of conditions used in an sql query.
    /// </summary>
    public enum Conjunction
    {
        /// <summary>
        /// <b>And</b>: The result is <c>true</c> if and only if <b>both</b> operands are <c>true</c>, otherwise <c>false</c>.
        /// </summary>
        [Description("AND")]
        And,

        /// <summary>
        /// <b>Or</b>: The result is <c>true</c> if at least one of the operands is <c>true</c> or both. It returns
        /// <c>false</c> if both operands are <c>false</c>.
        /// </summary>
        [Description("OR")]
        Or
    }
}
