﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Logging;
using System;
using System.ComponentModel;
using System.Data;

namespace Frank.Database.Connection
{
    /// <summary>
    /// Base class for database connections.
    /// </summary>
    public abstract class DatabaseConnection
    {
        /// <summary>
        /// Database connection.
        /// </summary>
        protected IDbConnection DbConnection
        {
            get;
            set;
        }

        /// <summary>
        /// Name of the database connection type.
        /// </summary>
        public string Name
        {
            get
            {
                DescriptionAttribute[] descriptionAttributes =
                    (DescriptionAttribute[])GetType().GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (descriptionAttributes.Length > 0) {
                    return descriptionAttributes[0].Description;
                }
                Logger.Warning("No description attribute found for type {0}, using type name instead.", GetType().FullName);
                return GetType().Name;
            }
        }

        /// <summary>
        /// Returns whether the database is connected.
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return DbConnection != null && DbConnection.State != ConnectionState.Closed;
            }
        }

        /// <summary>
        /// Begin transaction with the given isolation level.
        /// </summary>
        /// <param name="isolationLevel">the isolation level</param>
        /// <returns></returns>
        public IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return DbConnection.BeginTransaction(isolationLevel);
        }

        /// <summary>
        /// Find the id of in the specified table with the given id column using the specified condition.
        /// Throws <c>ArgumentException</c> if select statement returns more than one line.
        /// </summary>
        /// <param name="schema">the name of the database schema ("dbo" is quite common in MS SQL databases).</param>
        /// <param name="table">the database table</param>
        /// <param name="condition">the condition to use in where clause.</param>
        /// <param name="idColumn">the id column</param>
        /// <param name="id">the id value</param>
        /// <returns><c>true</c> if id was found, <c>false</c> otherwise.</returns>
        public bool FindId(string schema, string table, Condition condition, string idColumn, out int id)
        {
            IDataReader reader = SelectWhere(schema, table, condition, idColumn);
            if (reader.RecordsAffected < 1) {
                id = -1;
                reader.Close();
                return false;
            }
            if (reader.RecordsAffected == 1) {
                reader.Read();
                id = reader.GetInt32(reader.GetOrdinal(idColumn));
                reader.Close();
                return true;
            }
            throw new ArgumentException("Condition is not exact enough", "condition");
        }

        /// <summary>
        /// Establish a connection to the specified server using the given name and password
        /// </summary>
        /// <param name="server">The server name or address</param>
        /// <param name="port">The server port</param>
        /// <param name="name">The user name</param>
        /// <param name="passowrd">The user's password</param>
        /// <param name="database">The database name</param>
        public abstract void Connect(string server, uint port, string name, string passowrd, string database);

        /// <summary>
        /// Select the given columns from the specified database table.
        /// </summary>
        /// <param name="schema">the name of the database schema ("dbo" is quite common in MS SQL databases).</param>
        /// <param name="table">The database table</param>
        /// <param name="columns">The columns</param>
        public abstract IDataReader Select(string schema, string table, params string[] columns);

        /// <summary>
        /// Select the given columns from the specified database table using the given where condition
        /// </summary>
        /// <param name="schema">the name of the database schema ("dbo" is quite common in MS SQL databases).</param>
        /// <param name="table">The database table</param>
        /// <param name="condition">The where condition</param>
        /// <param name="columns">The columns</param>
        /// <returns></returns>
        public abstract IDataReader SelectWhere(string schema, string table, Condition condition, params string[] columns);

        /// <summary>
        /// Insert the given values into the specified database table.
        /// </summary>
        /// <param name="schema">the name of the database schema ("dbo" is quite common in MS SQL databases).</param>
        /// <param name="table">The database table</param>
        /// <param name="values">The tuples representing column name and the value for that column</param>
        public abstract int Insert(string schema, string table, params Tuple<string, object>[] values);

        /// <summary>
        /// Update the values in the specified database table using the given values.
        /// </summary>
        /// <param name="schema">the name of the database schema ("dbo" is quite common in MS SQL databases).</param>
        /// <param name="table">The database table</param>
        /// <param name="values">The values</param>
        public abstract void Update(string schema, string table, params object[] values);

        /// <summary>
        /// Close the database connection
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Gets whether the database connection accepts system login data.
        /// </summary>
        public abstract bool AcceptSystemLogin
        {
            get;
        }
    }
}