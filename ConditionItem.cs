﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Reflection;

namespace Frank.Database.Connection
{
    /// <summary>
    /// A single condition item in the form "key = parameter", 
    /// where '=' may be any of the values defined by the <see cref="Comparision"/> enumeration.
    /// </summary>
    public class ConditionItem : IConditionItem
    {
        /// <summary>
        /// Build a new condition item
        /// </summary>
        /// <param name="key">The key</param>
        /// <param name="parameter">The parameter</param>
        /// <param name="comparision">The comparision to use</param>
        /// <param name="value">The value for the parameter</param>
        public ConditionItem(string key, string parameter, Comparison comparision, object value)
        {
            Key = key;
            Parameter = parameter;
            Comparision = comparision;
            Value = value;
        }

        /// <summary>
        /// The comparison
        /// </summary>
        public Comparison Comparision
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the key
        /// </summary>
        public string Key
        {
            get;
            private set;
        }

        /// <summary>
        /// Get the parameter
        /// </summary>
        public string Parameter
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the value
        /// </summary>
        public object Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Get the string representation of the comparison
        /// </summary>
        /// <returns></returns>
        public string GetComparisionString()
        {
            MemberInfo[] memberInfo = Comparision.GetType().GetMember(Comparision.ToString());
            object[] attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            return ((DescriptionAttribute)attributes[0]).Description;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return String.Format("{0}{1}{2}", Key, GetComparisionString(), Parameter);
        }
    }
}
